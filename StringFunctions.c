#include <stdio.h>
#include <string.h>

//Definitions
#define SIZE 20

//Flags
int DONEflag = 0;   //Our flag to terminate the loop and exit the program

//Main function
int main()
{
    //Creates the long string
    char mommyString[1000];
    for(int i = 0; i < sizeof(mommyString); i++)
    {
        mommyString[i] = '\0';
    }
    
    //Our loop to continue unless the user toggles the flag by typing DONE
    while(DONEflag != 1)
    {
        //our tiny string, we only want 20 chars for this one
        char babystring[SIZE];
        for(int i = 0; i < SIZE; i++)
        {
            babystring[i] = '\0';
        }
        
        //prompt
        printf("Enter a String or type 'DONE' to exit the program: ");
        
        //snatches keyboard input for us and puts it into the string
        fgets(babystring, SIZE, stdin );
        
        //finds the last char in the string
        int length = strlen(babystring);
        
        //replaces the newline with a null
        babystring[length - 1] = '\0';
        
        //Compares our string to make sure that it isn't our exit function
        int DONEcheck = strcmp(babystring, "DONE");
        
        //Searches the main string for the short string
        char *find;
        find = strstr(mommyString, babystring);
        
        //checks to see if our main string is empty
        if(mommyString[0] == '\0')
        {
            //copies first word without a space
            strcat(mommyString, babystring);
        }
        else
        {
            if(DONEcheck != 0)
            {
                if(find == NULL)
                {
                    int Compare = strcmp(mommyString, babystring);
                    
                    //If string is greater than mainString
                    if(Compare > 0)
                    {
                        //puts a space at the end of the string and then adds babystring
                        strcat(mommyString, " ");
                        strcat(mommyString, babystring);
                    }
                    //else if mainString is greater than string
                    if(Compare < 0)
                    {
                        //Shifting the array to the right
                        for(int i = strlen(mommyString); i >= 0; i--)
                        {
                            mommyString[i + length] = mommyString[i];
                        }
                        strcpy(mommyString, babystring);
                        mommyString[length - 1] = ' ';
                       
                    }
                }
            }
            else
            {
                DONEflag = 1;
            }
        }
        printf("MainString in the loop: %s\n", mommyString);
    }
    
    printf("MainString: %s\n", mommyString);
}
